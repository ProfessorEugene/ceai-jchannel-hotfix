## PortletConfig "MulticastSimpleCache" hotfix application ##
This is a webapp that can be used to fix a memory leak issue with CampusEAI's portlet-config-1.1.9.  

This fix will only work on:

+ mycampus instances that have jgroups 2.8.1.GA installed
+ mycampus instances that keep portlet-config 1.1.9 jar in the tomcat /lib directory

### Building ###
* obtain access to a unix-y machine
* obtain JDK7
* obtain apache maven
* run ```mvn package```
* obtain war file from ```target/jchannel-hotfix.jar```

### How to use ###
* Either build this webapp using the above or just download it directly from [here](https://bitbucket.org/ProfessorEugene/ceai-jchannel-hotfix/downloads/jchannel-hotfix.war)
* Simply deploy this webapp on all affected servers
* After deployment you can visit http://<server>:<port>/jchannel-hotfix/hotfixInfo to see hotfix results in JSON format

### Problem Description ###
* ```portlet-config-1.1.9``` is packaged directly into ```/opt/mycampus/current/lib/portlet-config.jar```; this means
    * all webapps with a standard classloader use a single instance of ```MulticastSimpleCache.java``` 
* There is a ```jgroups.jar``` containing jgroups ver. 2.8.1.GA in ```/opt/mycampus/current/lib/jgroups.jar```; this means
    * all webapps will a standard classloader use jgroups v.2.8.1
* JGroups 2.8.1.GA *never* cleans up its message queue unless a receiver is attached to the channel (see [here](https://github.com/belaban/JGroups/blob/3abee7f48aef3d21a35eb01935f0cb772b19d248/src/org/jgroups/JChannel.java#L756)).  This means that messages on a JChannel are *never* cleaned up and stick around forever, eating up space.
* ```MulticastSimpleCache.java``` creates *two* JChannel instances - one used for invalidation and one used for publishing invalidation messages
    * The invalidation listening channel called "channel" has a receiver and thus its queue does not cause a problem
    * The invalidation publishing channel called "invalidationPublishchannel" on the other hand does not have a receiver and collects messages indefinitely.  There millions of tiny messages that can't get garbage collected, causing a full old-gen and constant gc.

### Fix Description ###
This fix simply obtains the global instance of ```MulticastSimpleCache``` that seems to be used by all instances of ```portlet-config``` and attaches an empty 
"Receiver" to it.  See [MSCHotfixer.java](src/main/java/org/campuseai/portletconfig/jchannelhotfix/MSCHotfixer.java) for operation.  This is obviously not an ideal
solution.  Unfortunately

* I don't have access to ```portlet-config``` sources
* ```portlet-config``` does not appear to follow a standard maven release process anymore

### Long term fix ###
* MulticastSimpleCache needs to be modified to either
    * just use a *single* JChannel for both publishing and receiving invalidation messages
    * add an empty receiver to MulticastSimpleCache
* portlet-config needs to be re-released and added to all applications as well as the tomcat /lib directory



