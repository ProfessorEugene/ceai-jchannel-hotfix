package org.campuseai.portletconfig.jchannelhotfix;

import junit.framework.Assert;
import org.campuseai.libs.portletconfig.cache.simplecache.impl.MulticastSimpleCache;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Tests for {@link MSCHotfixer}
 */
public class MSCHotfixerTest {
    private static MulticastSimpleCache multicastSimpleCache;

    @BeforeClass
    public static void setUp() throws IOException {
        /* use ipv4 */
        System.setProperty("java.net.preferIPv4Stack", "true");
        /* try to create a default "liferay-tcp.xml" file if one doesn't exist */
        File jGroupsConfigFile = new File("/apps/mycampus/conf/sharedcp/classes/myehcache/liferay-tcp.xml");
        if (!jGroupsConfigFile.exists()) {
            jGroupsConfigFile.getParentFile().mkdirs();
            InputStream sampleConfigFileStream = MSCHotfixerTest.class.getClassLoader().getResourceAsStream("liferay-tcp.xml");
            FileOutputStream configFileOutputStream = new FileOutputStream(jGroupsConfigFile);
            int b = 0;
            while ((b = sampleConfigFileStream.read()) != -1) {
                configFileOutputStream.write(b);
            }
            configFileOutputStream.flush();
        }
        multicastSimpleCache = MSCUtil.getInstance();
    }

    @Test
    public void test() throws Exception {
        /* add some messages to the perma-queue */
        for (int i = 0; i < 1000; i++) {
            multicastSimpleCache.put("test-key", "test-value");
        }
        assertEquals(1001, MSCUtil.getInvalidationChannel(multicastSimpleCache).getNumMessages());
        /* fix ! */
        MSCHotfixResult fixResult = new MSCHotfixer().applyFix();
        /* verify results */
        assertNull(fixResult.getException());
        assertEquals(1002,fixResult.getDiscardedMessages());
        /* verify that all messages have been discarded */
        assertEquals(0,MSCUtil.getInvalidationChannel(multicastSimpleCache).getNumMessages());
        /* add a bunch more messages */
        for (int i = 0; i < 1000; i++) {
            multicastSimpleCache.put("test-key", "test-value");
        }
        /* verify all new messages have been discarded */
        assertEquals(0,MSCUtil.getInvalidationChannel(multicastSimpleCache).getNumMessages());
    }
}
