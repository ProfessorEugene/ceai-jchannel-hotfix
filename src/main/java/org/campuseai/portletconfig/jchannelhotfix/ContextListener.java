package org.campuseai.portletconfig.jchannelhotfix;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * A {@code ServletContextListener} that attempts to hot-patch shared {@code MulticastSimpleCache} instances on context
 * startup
 */
public class ContextListener implements ServletContextListener {
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        servletContextEvent.getServletContext().setAttribute(MSCUtil.MSC_KEY, new MSCHotfixer().applyFix());
    }

    public void contextDestroyed(ServletContextEvent servletContextEvent) {
    }
}
