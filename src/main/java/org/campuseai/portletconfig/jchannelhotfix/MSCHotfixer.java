package org.campuseai.portletconfig.jchannelhotfix;

import org.campuseai.libs.portletconfig.cache.simplecache.impl.MulticastSimpleCache;
import org.jgroups.JChannel;
import org.jgroups.ReceiverAdapter;
import org.jgroups.TimeoutException;

/**
 * A {@code MulticastSimpleCache} hot fix utility
 */
public class MSCHotfixer {

    public MSCHotfixResult applyFix() {
        /* create a result */
        MSCHotfixResult result = new MSCHotfixResult().setHotfixTimestamp(System.currentTimeMillis());
        try {
            /* pull an instance of cache */
            MulticastSimpleCache cache = MSCUtil.getInstance();
            /* attempt to initialize the cache */
            MSCUtil.init(cache);
            /* pull invalidation publish channel from cache */
            JChannel invalidationPublishchannel = MSCUtil.getInvalidationChannel(cache);
            /* check whether cache has already been hotfixed */
            if (invalidationPublishchannel.getReceiver() == null) {
                /* dump all messages */
                while (invalidationPublishchannel.getNumMessages() > 0) {
                    try {
                        invalidationPublishchannel.receive(1);
                    } catch (TimeoutException to) {

                    }
                    result.setDiscardedMessages(result.getDiscardedMessages() + 1);
                }
                /* register an empty listener */
                invalidationPublishchannel.setReceiver(new ReceiverAdapter());
            }
        } catch (Exception exception) {
            result.setException(exception);
        }
        return result;
    }
}
