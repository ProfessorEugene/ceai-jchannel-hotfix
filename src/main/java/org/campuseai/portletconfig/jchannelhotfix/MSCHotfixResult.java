package org.campuseai.portletconfig.jchannelhotfix;

/**
 * Hotfix result class
 */
public class MSCHotfixResult {
    long hotfixTimestamp;
    long discardedMessages;
    Exception exception;

    public long getHotfixTimestamp() {
        return hotfixTimestamp;
    }

    public MSCHotfixResult setHotfixTimestamp(long hotfixTimestamp) {
        this.hotfixTimestamp = hotfixTimestamp;
        return this;
    }

    public long getDiscardedMessages() {
        return discardedMessages;
    }

    public MSCHotfixResult setDiscardedMessages(long discardedMessages) {
        this.discardedMessages = discardedMessages;
        return this;
    }

    public Exception getException() {
        return exception;
    }

    public MSCHotfixResult setException(Exception exception) {
        this.exception = exception;
        return this;
    }
}
