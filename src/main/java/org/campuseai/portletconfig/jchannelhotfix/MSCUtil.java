package org.campuseai.portletconfig.jchannelhotfix;

import org.campuseai.libs.portletconfig.cache.simplecache.impl.MulticastSimpleCache;
import org.jgroups.JChannel;

import java.lang.reflect.Field;

/**
 * Utility class for dealing with {@code MulticastSimpleCache}
 *
 */
public class MSCUtil {
    public static String MSC_KEY = "mscHotfixResult";
    /**
     * Obtain a {@code MulticastSimpleCache} instance from the current context classloader
     * @return static instance of {@code MulticastSimpleCache}
     */
    public static MulticastSimpleCache getInstance(){
        return MulticastSimpleCache.instance;
    }

    /**
     * Initialize a {@code MulticastSimpleCache} instance by placing something in the cache
     * @param msc a {@code MulticastSimpleCache} instance
     */
    public static void init(MulticastSimpleCache msc){
        msc.put("_completely_useless_key_","temp_value");
    }


    /**
     * Get the "invalidationPublishchannel" field value from a {@code MulticastSimpleCache} instance
     * @param msc an instance of {@code MulticastSimpleCache}
     * @return invalidationPublishchannel from a {@code MulticastSimpleCache}
     * @throws Exception
     */
    public static JChannel getInvalidationChannel(MulticastSimpleCache msc) throws Exception{
        Field invalidationPublishchannelField =
                msc.getClass().getDeclaredField("invalidationPublishchannel");
        invalidationPublishchannelField.setAccessible(true);
        return (JChannel)invalidationPublishchannelField.get(msc);
    }
}
