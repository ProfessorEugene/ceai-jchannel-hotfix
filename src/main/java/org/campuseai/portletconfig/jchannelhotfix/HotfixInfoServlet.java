package org.campuseai.portletconfig.jchannelhotfix;

import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet that dumps some information on MSC hotfix
 */
public class HotfixInfoServlet extends HttpServlet {
    private Gson gson = new Gson();

    public static class HotfixInfo {
        MSCHotfixResult hotfixResult;
        int currentQueueSize;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            HotfixInfo info = new HotfixInfo();
            info.currentQueueSize = MSCUtil.getInvalidationChannel(MSCUtil.getInstance()).getNumMessages();
            info.hotfixResult = (MSCHotfixResult) getServletContext().getAttribute(MSCUtil.MSC_KEY);
            resp.setContentType("application/json");
            resp.getOutputStream().println(gson.toJson(info));
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }
}
